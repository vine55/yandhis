<div align="center">

![Yandhi](https://ik.imagekit.io/meqsbz7rhm/Sem_T_tulo-4_gU-Dvc2CE.jpg)
</div>
 
 
 
 # Indice
 - [Sobre](#-sobre)
 - [Tecnologias Utilizadas](#-tecnologias-utilizadas)
 - [Como baixar o projeto](#-como-baixar-o-projeto) 
 - [Descrevendo Yandhi](#-descrevendo-yandhi)
 - [Pontos fortes e fracos](#-pontos-fortes-e-fracos)  
 - [Considerações finais](#-considerações-finais)

 
 ## 📘 Sobre

  O projeto Yandhi é um Robo de guerra virtual criado no Robocode, iniciado durante o Solutis - Talent Sprint 2020 que tem a intenção de encontrar novos talentos desenvolvedores.

---

 ## 🚀 Tecnologias Utilizadas

 O projeto foi desenvolvido completamente em C#, criado em uma biblioteca de .NET Framwork

 - [C#](https://docs.microsoft.com/pt-br/dotnet/csharp)
 - [Robocode](https://robocode.sourceforge.io/)

---

 ## 🗂️ Como baixar o projeto

  ### É possivel baixar por zip direto do git.

   Só entrar na pagina: https://gitlab.com/vine55/yandhis
   
   Cliclar no icone de Download e escolher a opção que seja melhor para você
   
  ![imagem](https://ik.imagekit.io/meqsbz7rhm/ddd_Vfyqhp4zr.jpg)
  
  
  
  
---

 ## 👾 Descrevendo Yandhi

 Yandhi é um robo agressivo porém sabe ser cauteloso, primeiramente ele faz um escaneamento da area em 360 graus até que encontre um inimigo, no momento que o adversario é definido Yandhi realiza uma investida contra ele.
 
 
 
 No momento que o adversario foi escaneado e localizado Yandhi verifica a distancia entre eles dois e avança até chegar em uma distancia especifica para começar a atirar.
 
 Assim que o adversario começa a se mover na direção de Yanhdi ele tenta manter uma distancia segura evitando colisões.

 Apartir de uma quantidade especifica de energia do robo inimigo se a energia de Yandhi for menor que a energia do adversario ele começa a ser mais cauteloso.



![Não toca em mim](https://media1.tenor.com/images/9c3e4244011bf4c80c8cea1d03df5561/tenor.gif?itemid=12078871)
 
 
 
 
 Se ele for atingido por tiros ele tenta se esquivar se mantendo em movimento, caso o inimigo esteja com a energia baixa Yanhdi entra em frenesi e vai com tudo para cima dele, não se importando muito com colisões porém tentando manter uma pequena distancia do outro robo.

 ![Frenesi](https://m.gifmania.pt/Gifs-Animados-Objetos/Imagens-Animadas-Armas/Gif-Animados-Machine-Guns/Machine-Guns-87936.gif)

 Caso fique preso ou encostado em alguma parede Yandhi consegue se livrar dessa situação indo para frente, se o inimigo estiver dentro da zona de investida de Yandhi ele prioriza esse movimento e sai da parede ou canto.

 Assim que o adversario morrer, Yandhi procura outro alvo, porém durante a batalha Yandhi alterna de alvos priorizando o que estiver mais proximo e com menos vida.


 ![É você pae](https://em.wattpad.com/0b6a3b2ec5b22dc2203a60c769571a3e9d99a355/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f776174747061642d6d656469612d736572766963652f53746f7279496d6167652f6b725675684d68475330774231413d3d2d3438333735313439302e313465656366343765316533323537653738383839313730383236382e676966)

 ## 💪👎 Pontos fortes e fracos


Pontos fortes: Consegue manter  distancia do inimigo quando necessario, agressivo, se movimenta bastante, está sempre rastreando o inimigo.

Pontos fracos: No inicio do combate e quando o inimigo está com pouca vida Yandhi avança muito a ponto de colidir varias vezes, quando tem um ou mais alvos dentro do radar ele pode ficar indeciso de quem vai focar ainda mais se ambos os adversarios estiverem com pouca vida.

 

 ## 🧠 Considerações finais.
 
 Sem duvidas a parte mais divertida foi configurar a movimentação dele, ter feito ele se tornar mais agressivo ao decorrer que a vida do inimigo for diminuindo foi bem legal de por em pratica. Foi bastante desafiador, não tinha tanta experiencia e mesmo assim consegui fazer algo que me deixou contente, trabalhar com o robocode é divertido e sem duvidas irei continuar me aventurando nele. 


 ---

 Desenvolvido por ☂️ Vinicius Almeida de Araujo

