﻿
using Robocode;
using System.Drawing;

namespace YandhiSS

{
    public class Yandhi : AdvancedRobot
    {
        
       private string enemy_name;
        public override void Run()
        {
            BodyColor = (Color.FromArgb(255, 255, 255));
            GunColor = (Color.Pink);
            RadarColor = (Color.Purple);
            ScanColor = (Color.White);

            enemy_name = null;
            while (true)
            {
                // ligando o radar em 360 graus e escaneando a area até encontrar um inimigo 
                TurnGunRight(360);
            }
        }
        //Quando estiver encontrado um inimigo Yandhi vai fazer
        public override void OnScannedRobot(ScannedRobotEvent e)
        {

            //Se o inimigo estiver longe Yandhi vai em direção dele
            if (e.Distance > 200)
            {
                //Mirando no inimigo
                SetTurnGunRight(Heading - RadarHeading + e.Bearing);
                SetTurnRight(e.Bearing);
                SetAhead(e.Distance - 70);
               
                //Se a distancia for menor que 50
                if (e.Distance < 50)
                {
                    // precisa se afastar do inimigo e evitar colisão
                    if (e.Bearing > -90 && e.Bearing <= 90)
                    {
                        Back(60);
                    }
                    else
                    {
                        Ahead(60);
                    }
                }

            }
            //Se Yandhi está numa distancia favoravel para começar a atirar
            else if (e.Distance < 200)  
            {
                //Se a temperatura da arma for igual a zero.
                if (GunHeat == 0)
                Fire(2);
            }
            //Caso o inimigo esteja com menos que 20 de energia
            if ((e.Energy < 20) && (e.Energy > Energy) && (e.Distance >= 0))
            {   
                //Se a temperatura da arma for igual a zero
                if (GunHeat == 0)
                Fire(3);
            }
            //Se a energia do aversario for menor que 70 e a energia do adversario for maior que a de Yandhi
            //ele irá se afastar
            if ((e.Energy < 70) && (e.Energy > Energy))
            {
                

                SetTurnGunRight(e.Bearing);

                if (e.Distance < 70)
                {
                    // precisa se afastar do inimigo e evitar o choque
                    if (e.Bearing > -90 && e.Bearing <= 90)
                    {
                        Back(60);
                    }
                    else
                    {
                        Ahead(60);
                    }
                }
                if (e.Distance > 200)
                {
                    //Se o inimigo estiver longe Yandhi vai em direção dele
                    SetTurnGunRight(Heading - RadarHeading + e.Bearing);
                    SetTurnRight(e.Bearing);
                    SetAhead(e.Distance - 70);
                }
                //Se Yandhi está numa distancia favoravel para começar a atirar
                else if (e.Distance < 200)
                {
                    //se a temperatura da arma for igual a zero
                    if (GunHeat == 0)
                        Fire(3);
                }
                // Se o inimigo estiver com 50 ou menos de energia Yandhi ataca mais forte
                if ((e.Energy <= 50) && (e.Energy > Energy) && (e.Distance >= 0))
                {
                    if (e.Distance > 200)
                    {
                        //Se o inimigo estiver longe Yandhi vai em direção dele
                        SetTurnGunRight(Heading - RadarHeading + e.Bearing);
                        SetTurnRight(e.Bearing);
                        SetAhead(e.Distance - 70);
                        if (e.Distance < 50)
                {
                   // tenta se afastar do inimigo e evitar colisão mas se mantendo perto
                    if (e.Bearing > -90 && e.Bearing <= 90)
                    {
                        Back(60);
                    }
                    else
                    {
                        Ahead(60);
                    }
                }
                    }
                    //Se Yandhi está numa distancia favoravel para começar a atirar
                    else if (e.Distance < 200)
                    {
                        //se a temperatura da arma for igual a zero
                        if (GunHeat == 0)
                            Fire(3);
                    }
                }

            }
            

            }

        public override void OnHitByBullet(HitByBulletEvent e)
        {
            //se for atingido tenta sair do lugar
            Ahead(100);
        }
        public override void OnRobotDeath(RobotDeathEvent evnt)
        {
            if (evnt.Name == enemy_name)
            {
                //quando o inimigo morrer troca de alvo
                enemy_name = null;
            }
        }

            //Caso fique colado em alguma parede Yandhi tenta sair
            void OnHitWall(HitWallEvent evnt)
        {
            SetTurnRight(evnt.Bearing);
            Back(100);
        }
    }
    }
    

